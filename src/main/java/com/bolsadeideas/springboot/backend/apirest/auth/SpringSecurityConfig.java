package com.bolsadeideas.springboot.backend.apirest.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

//habilitando para permitir anotaciones de permisos para un controlador
@EnableGlobalMethodSecurity(securedEnabled=true)
@Configuration
public class SpringSecurityConfig  extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserDetailsService usuDetailsService;

	@Override
	@Autowired
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(this.usuDetailsService).passwordEncoder(passwordEncoder());
	}
	
	//registra en el contendedor de SPRING 
	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean("authenticationManager")
	@Override
	protected AuthenticationManager authenticationManager() throws Exception {
		// TODO Auto-generated method stub
		return super.authenticationManager();
	}
	
	//reglas de seguridad de todos nuestros endpoints , rutas hacia nuestro recursos de nuestra aplicacion
		@Override
		public void configure(HttpSecurity http) throws Exception {
			//.4to-.5to  se coloca al final de toda regla 
			//csrf() desahbilitar la proteccion CSRF: Cross-site request forgery o 
			//falisifiacion de peticion en sitios cruzados
			// dejar sin estado(stateless) deshabilitamos el manejo de sesiones del lado del servidor
			http.authorizeRequests().
			anyRequest().authenticated()//se 
			.and().
			csrf().disable().
			sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
			
			
		}
	
	
	
	
}
