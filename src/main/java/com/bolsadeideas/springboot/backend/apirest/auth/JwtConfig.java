package com.bolsadeideas.springboot.backend.apirest.auth;

public class JwtConfig {
	public static final String LLAVE_SECRETA = "alguna.clave.secreta.12345678";
	public static final String RSA_PRIVATE = "-----BEGIN RSA PRIVATE KEY-----\r\n" + 
			"MIIEpQIBAAKCAQEA0EPiKhffUvn/DBR5PoB3fzhRAhc5ISk0Al2CU4+BhP+QkEYD\r\n" + 
			"BOIF90E1rcXGKumYEBF52TRpSisvMPWRmQnz6nKB6xLREj5eEZHs049iy7I+YpqN\r\n" + 
			"uV8L1tnuk65nseygBtfgo/mKqs4LvX2s5PpH+ALPi8trJQe+MUD4LXf8WCnA35Ks\r\n" + 
			"jlAPPAsnLvpEW4bQCkm7tQSG+s5Qqbp09azN7/z+qAb5Oi3JaU+c8F6oXItwpbox\r\n" + 
			"TDudrMo2HgNHeus3gkGKE710vdIdfwIXiVQ33/2lQ9n9tEDlmK/s/+WgA9+U/54j\r\n" + 
			"9vqPxHwSm24qNjLfhaUC5wvUU4MaMeOIQjGkSwIDAQABAoIBACkw4/BzXEYrD9ix\r\n" + 
			"y/Eis3/EAbGqKjB218SH/hjMz08gxNTkJAC5O3jVuuotra4bGWpEKpddGF4FAIZs\r\n" + 
			"a2mbYFTEZRG7XQKAt1Y+SFrnhyDGljQmYVWCiPLrk5Y97mBwbo4r8FA+MMDrk8Lc\r\n" + 
			"sm9vWQCifa4mg4CFcYaIVQ2ZJ52/+NImnTshyJEOFAPG0Hk9mVGq+y6I7spMGrcx\r\n" + 
			"2JPae/XPEmvVygfgbTXuTZzFVdAmt6FOMf9q1yfnuF8ANLU+fnht5RDowoCAcdA9\r\n" + 
			"pLnhx0HDTGxl1VdVtfeFzwxxYpqfGaDFtu+KoN97M9eoxocw0pIZib5IuF4+OlIJ\r\n" + 
			"fOkh4WECgYEA9gkHFs+Km9PM9TgTp9DSmoNOKqw3pzzVGkAqipRJBG+Ex1tnVNcM\r\n" + 
			"evBYtrWQDZ4i8HMwtcPqgev3rljgXhu9WKqSqxzo7asPNDbSgtVj/5oLpy5Ih7xb\r\n" + 
			"Pz9BL1yNIc7ZPeppXtDAkklpNO4H/gN7MCM2sWgiVPZIH1HP/E2go9kCgYEA2LM+\r\n" + 
			"S6Xn3IAWmE/gzM9Qa7xhf9yEuH6EyStCa7/GJV+Lr1yT1ZJIvuH3GxlfC3KY2mpG\r\n" + 
			"bNiam1LgWZSKNcabLD6S1ftWLbL+lvNFe/w2qlvHIZnPNGAJuiduDthivgoD2MOc\r\n" + 
			"qTzKKEHWKuwnDD5DrZa6FSSlMGJuwOJKyeETxsMCgYEAjJbCB0XW9Y5EuwctLCLG\r\n" + 
			"hWZsnzPDpXu8ZgCujnuHcdJrtCNF25P63LFEcDs1sqJfYrjf562o7k6xPF4q73u4\r\n" + 
			"adcmckLV0yGBUZvWFM7tElyrNf/bkDyQg4aYsACZ4Oy69IdjaLuunqj2RjmSNZM1\r\n" + 
			"rF8i3KMdoyEMO2FbkNem6JkCgYEAvgXoAbQZLw2PQr8A9PhyoBbQNKwuIg6n6DQs\r\n" + 
			"CYpgehNfzUXUSTr+2YwTAUttQf+atCTvXrFOWhgBrewIioF/PvwcDNJaTragc8Qd\r\n" + 
			"0Uxor1jNtNRIk7gHr5q18wSjCdyEZQebj/hDmaup33e4Ag9zj3wV70Z0KIqYbkHB\r\n" + 
			"rWfWzLsCgYEAwBobYMWDFspKjOcIqdXs2vaOBQG0Cxpv42ZwRTkZDVweHclqJteT\r\n" + 
			"mq7k4MdoH+QCbIUO2k8Tls/XVEARVZOcMNt1XUxRPrzZ2Kr+wKu+kirvzQxWXb4p\r\n" + 
			"/PhyV4UWzk3bsx4JB9gYdGxCIIDYKB/I6zCQmqJJvXeqO3m9h6FcN6M=\r\n" + 
			"-----END RSA PRIVATE KEY-----";
	
	public static final String RSA_PUBLIC = "-----BEGIN PUBLIC KEY-----\r\n" + 
			"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA0EPiKhffUvn/DBR5PoB3\r\n" + 
			"fzhRAhc5ISk0Al2CU4+BhP+QkEYDBOIF90E1rcXGKumYEBF52TRpSisvMPWRmQnz\r\n" + 
			"6nKB6xLREj5eEZHs049iy7I+YpqNuV8L1tnuk65nseygBtfgo/mKqs4LvX2s5PpH\r\n" + 
			"+ALPi8trJQe+MUD4LXf8WCnA35KsjlAPPAsnLvpEW4bQCkm7tQSG+s5Qqbp09azN\r\n" + 
			"7/z+qAb5Oi3JaU+c8F6oXItwpboxTDudrMo2HgNHeus3gkGKE710vdIdfwIXiVQ3\r\n" + 
			"3/2lQ9n9tEDlmK/s/+WgA9+U/54j9vqPxHwSm24qNjLfhaUC5wvUU4MaMeOIQjGk\r\n" + 
			"SwIDAQAB\r\n" + 
			"-----END PUBLIC KEY-----";
}
