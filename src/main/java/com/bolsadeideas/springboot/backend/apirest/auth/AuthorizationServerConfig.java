package com.bolsadeideas.springboot.backend.apirest.auth;

import java.util.Arrays;

//import org.aspectj.weaver.Dump.INode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
//import org.springframework.security.oauth2.provider.token.TokenStore;
//import org.springframework.security.oauth2.provider.token.AccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;



@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfig  extends AuthorizationServerConfigurerAdapter{
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	@Autowired
	@Qualifier("authenticationManager")
	private AuthenticationManager authenticationManager;
	
	@Autowired
	private InfoAdicionalToken infoAdicionalToken;

	@Override
	//se configura los permisos de nuestras rutas de accesos 
	public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
		///1ro generar el token  que el usuario quien se autentica,con permitAll() damos acceso a cualquier usuario 
		//2do parametros es dara permito al endpoint que se encarga de validar el webtoken que se esta enviendo ,solo pueden acceder los uarios autenticados
		security.tokenKeyAccess("permitAll()").
		checkTokenAccess("isAuthenticated()");
	}

	@Override
	public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
		//es en esta configuracion donde podemos poner las distintos acceso de aplicaciones tales como android , react ,etc
		//primero registramos el tipo de almacenamiento
		//en segundo lugar se va registrar id , corresponde el user o aplicacion cliente
		//en tercer parametros va la contraseña 
		//el 4to parametro es el alcance o scope permiso que va tener el cliente 
		//el 5to asignamos el tipo de autenticacion de nuestra aplicacion , sirve para el acceso de los usaurios
		//refreash_token nos permite obtener un token renovado
		//el 6to parametro es el tipo de caducidad del token /  
		clients.inMemory().withClient("angularapp").secret(passwordEncoder.encode("12345"))
		.scopes("read","write").
		authorizedGrantTypes("password","refresh_token").
		accessTokenValiditySeconds(3600).
		refreshTokenValiditySeconds(3600);
		
		;
	}

	@Override
	public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
		TokenEnhancerChain tokenEnhancerChain = new TokenEnhancerChain();
		tokenEnhancerChain.setTokenEnhancers(Arrays.asList(infoAdicionalToken,accessTokenConverter()));
		
		
		endpoints.authenticationManager(authenticationManager).
		tokenStore(tokenStore()).
		accessTokenConverter(accessTokenConverter());
	}
	
	@Bean
	public JwtTokenStore tokenStore() {
		return new JwtTokenStore(accessTokenConverter());
	}
	

	@Bean("accessTokenConverter")
	public JwtAccessTokenConverter accessTokenConverter() {
		JwtAccessTokenConverter jwtAccessTokenConverter = new JwtAccessTokenConverter();
		//firmante la llave privada
		jwtAccessTokenConverter.setSigningKey(JwtConfig.RSA_PRIVATE);
		//EL QUE VERIFICA ES LA LLAVE PUBLICA
		jwtAccessTokenConverter.setVerifierKey(JwtConfig.RSA_PUBLIC);
		
		return jwtAccessTokenConverter;
	}
	
	

}
