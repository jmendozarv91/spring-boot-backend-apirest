package com.bolsadeideas.springboot.backend.apirest.auth;

import java.util.Arrays;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

//124. Añadiendo la configuracion para el servidor de recurso 
/*
* ResourceServer --> se encarga de brindar acceso a los usuarios siempre y cuuando el token sea valido
* 
* */

@Configuration
@EnableResourceServer
public class ResourceServerConfig  extends ResourceServerConfigurerAdapter{

	//reglas de seguridad de todos nuestros endpoints , rutas hacia nuestro recursos de nuestra aplicacion
	@Override
	public void configure(HttpSecurity http) throws Exception {
		//.2do indicamos todas nuestras rutas,
		//.3er permite el acceso a todos los usuarios
		//.4to-.5to  se coloca al final de toda regla 
		http.authorizeRequests().antMatchers(HttpMethod.GET,"/api/clientes","/api/clientes/page/**","/api/uploads/img/**","/images/**").permitAll().
//		antMatchers("/api/clientes/{id}").permitAll().
//		antMatchers("/api/facturas/**").permitAll().
		/*antMatchers(HttpMethod.GET,"/api/clientes/{id}").hasAnyRole("USER","ADMIN").
		antMatchers(HttpMethod.POST,"/api/clientes/upload").hasAnyRole("USER","ADMIN").
		antMatchers(HttpMethod.POST,"/api/clientes").hasRole("ADMIN").
		antMatchers("/api/clientes/**").hasRole("ADMIN").*/
		anyRequest().authenticated()//se
		.and().cors().configurationSource(corsConfigurationSource());
	}
	
	@Bean
	public CorsConfigurationSource corsConfigurationSource() {
		CorsConfiguration config = new CorsConfiguration();
		// * --> Aceptaremos cualquier origen , lo recomendable es poner el servidor
		config.setAllowedOrigins(Arrays.asList("http://localhost:4200","*"));
		config.setAllowedMethods(Arrays.asList("GET","POST","PUT","DELETE","OPTIONS"));
		config.setAllowCredentials(true);
		config.setAllowedHeaders(Arrays.asList("Content-type","Authorization"));
		UrlBasedCorsConfigurationSource source =new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", config);
		return source;
	}
	
	@Bean FilterRegistrationBean<CorsFilter> corsFilter(){
		FilterRegistrationBean<CorsFilter> bean = new FilterRegistrationBean<CorsFilter>(new CorsFilter(corsConfigurationSource()));
		bean.setOrder(Ordered.HIGHEST_PRECEDENCE);
		return bean;
	}

	
	
}
